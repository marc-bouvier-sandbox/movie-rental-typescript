# Movie Rental

This source code follows Martin Fowler's book "Refactoring, Improving the Design of Existing Code".

When you find you have to add a feature to a program, and the program's code is not
structured in a convenient way to add the feature, first refactor the program to make it
easy to add the feature, then add the feature.

Whenever you do refactoring, the first step is always the same.
You need to build a solid set of tests for that section of code. The tests are essential because even
though you follow refactorings structured to avoid most of the opportunities for introducing bugs,
you are still human and still make mistakes. Thus you need solid tests.

Actualy the `statement` method prints out a simple text output of a rental statement
```
Rental Record for martin
  Ran 3.5
  Trois Couleurs: Bleu 2
Amount owed is 5.5
You earned 2 frequent renter points
```
We want to write an HTML version of the statement method :
```
<h1>Rental Record for <em>martin</em></h1>
<table>
  <tr><td>Ran</td><td>3.5</td></tr>
  <tr><td>Trois Couleurs: Bleu</td><td>2</td></tr>
</table>
<p>Amount owed is <em>5.5</em></p>
<p>You earned <em>2</em> frequent renter points</p>
```

## Usage

The purpose of this is to provide good examples for the refactoring workshop.

## Build

All you need to build this project is nodejs.

## Testing

Unit tests can be run using npm:

```shell
npm test
```

or 

```shell
npm test -- --watch
```

or

```shell
npm run testw
```

Tests are located in the src directory and run using jest.

## Test branch coverage

```shell
npm run testc
```

High branch coverage means that when running tests, the code was run in some conditional branches or not.
It does not mean that there was any test assertion to check for correct behaviour.

Branch code coverage can be useful to detect unused code or code that is never reached when running your program. 
In this case, you can ask yourself if such code can be removed.

Therefore, it is possible to have very high branch coverage without any test assertions executed.

As an experiment, try to remove `expect(customer.statement()).toBe(expected);` from [src/customer.test.ts](src/customer.test.ts), your coverage will still stay high. 

## Mutation testing with Stryker

Mutation allows you to check if your test coverage is not lying to you.
As mentionned before, you can reach 100% of branch code coverage without any test assertion.

Mutation testing works by mutating your code in some locations and verifies that at least one test fails.
For instance it may replace `if (value < 1)` with `if (value <= 1)`.
Then, it will run all tests with this change.
If no test breaks, it means that the regression was not spotted by you test suites.

Maybe you may need to add some new test to cover this hole in your suite.

- https://blog.ippon.fr/2020/05/20/le-mutation-testing-ou-comment-tester-ses-tests/
- https://stryker-mutator.io/ (Js, Ts, Scala, C#)
- https://pitest.org/ (Java)

If both your branch code coverage your mutation testing score are the more you can trust your tests to detect regressions. 